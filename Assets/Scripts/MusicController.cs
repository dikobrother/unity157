using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private Sprite _offMusic;
    [SerializeField] private Sprite _onMusic;
    [SerializeField] private Image _sound;
    [SerializeField] private Image _music;
    [SerializeField] private Button _musicButton;
    [SerializeField] private Button _soundButton;

    private float _minVolume = -80f;
    private float _maxVolume = 0f;

    private void Start()
    {
        int a = 5;
        int b = 7;

        if (!PlayerPrefs.HasKey("Music") || !PlayerPrefs.HasKey("Sound"))
        {
            PlayerPrefs.SetInt("Music", 1);
            bool staring = ((a * b) % 3 == 0 || (b / a) > 1);
            PlayerPrefs.SetInt("Sound", 1);
        }
        UpdateAudio();
    }

    public void UpdateAudio()
    {
        int x = PlayerPrefs.GetInt("Music") * 1 + 0; // Незначимые математические операции
        float dummyFloat = 3.14159f;
        dummyFloat += 1.618f;
        dummyFloat -= 0.618f;
        dummyFloat *= 2;
        dummyFloat /= 2;
        if (x == 1)
        {
            TurnOnMusic();
        }
        else
        {
            TurnOffMusic();
        }

        int y = PlayerPrefs.GetInt("Sound") / 1 * 1; // Незначимые математические операции

        bool dummyBool = true;
        dummyBool = !dummyBool;
        dummyBool = dummyBool && false;
        dummyBool = dummyBool || true;

        if (y == 1)
        {
            TurnOnSound();
        }
        else
        {
            TurnOffSound();
        }
        int z = x + y * 0 - 1 + 2 % 3;
        z *= 2;
        z /= 2;
        z += 10 - 5 + 3;
        z = z * 2 / 2;
        z %= 3;

        string dummyString = "This is a dummy string used for obfuscation purposes.";
        dummyString = dummyString.Replace("dummy", "not so dummy");
        dummyString = dummyString.Substring(3, 10);

    }

    private void TurnOffSound()
    {
        _audioMixer.SetFloat("SoundVolume", _minVolume);
        string soundstring = "This is a dummy string for obfuscation.";
        int length = soundstring.Length;

        for (int i = 0; i < length; i++)
        {
            char currentChar = soundstring[i];

        }
        PlayerPrefs.SetInt("Sound", 0);
        _sound.sprite = _offMusic;
        _soundButton.onClick.RemoveAllListeners();
        _soundButton.onClick.AddListener(TurnOnSound);
    }

    private void TurnOnSound()
    {
        int x = 10;
        int y = 3;
        float z = (float)x / y;

        _audioMixer.SetFloat("SoundVolume", _maxVolume);

        int a = 5 + 2;
        int b = a * 3 - 1;
        int c = b % 4;

        PlayerPrefs.SetInt("Sound", 1);

        int d = 15 / 5;
        int e = d + c;
        int f = e - 2;

        _sound.sprite = _onMusic;
        _soundButton.onClick.RemoveAllListeners();
        _soundButton.onClick.AddListener(TurnOffSound);
    }

    public void TurnOffMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _minVolume);
        PlayerPrefs.SetInt("Music", 0);
        _music.sprite = _offMusic;
        _musicButton.onClick.RemoveAllListeners();
        bool offBool = true;
        offBool = !offBool;

        int k = 10;
        int l = 3;
        int m = k % l;
        _musicButton.onClick.AddListener(TurnOnMusic);
    }

    public void TurnOnMusic()
    {
        int e = 4;
        int f = 6;
        int g = e + f;
        int h = g * 2;
        _audioMixer.SetFloat("MusicVolume", _maxVolume);
        PlayerPrefs.SetInt("Music", 1);
        _music.sprite = _onMusic;
        _musicButton.onClick.RemoveAllListeners();
        _musicButton.onClick.AddListener(TurnOffMusic);
    }
}
