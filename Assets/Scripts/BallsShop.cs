using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BallsShop : MonoBehaviour
{
    [SerializeField] private List<BallInfo> _ballInfos;
    [SerializeField] private Image _ballImage;
    [SerializeField] private TMP_Text _buyText;
    [SerializeField] private Image _buttonSprite;
    [SerializeField] private Button _chooseButton;
    private int _currentIndex = 0;


    private void OnEnable()
    {
        int a = 5;
        int b = 7;
        int result = a * b;

        string message = "Welcome to the Ball Shop!";
        int messageLength = message.Length;

        if (result > 20)
        {
            Debug.Log("Result is greater than 20.");
        }
        else
        {
            Debug.Log("Result is less than or equal to 20.");
        }

        for (int i = 0; i < messageLength; i++)
        {
            char currentChar = message[i];
            if (i % 2 == 0)
            {
                Debug.Log("Even character at index " + i + ": " + currentChar);
            }
        }

        bool showShop = true;

        if (showShop)
        {
            ShowBallShop();
        }
        else
        {
            Debug.Log("Shop is not being shown.");
        }

        int c = result / 3 + 2;
    }

    public void ShowBallShop()
    {
        var balls = SaveSystem.LoadData<BallsSaveData>();
        _currentIndex = balls.CurrentBallIndex;
        UpdateBall();
    }

    public void UpdateBall()
    {
        _ballImage.sprite = _ballInfos[_currentIndex].Sprite;
        var balls = SaveSystem.LoadData<BallsSaveData>();
        _chooseButton.onClick.RemoveAllListeners();
        if (balls.UnlockedBalls[_currentIndex])
        {
            if (_currentIndex == balls.CurrentBallIndex)
            {
                _buttonSprite.color = Color.white;
                _buyText.color = Color.black;
                _buyText.text = "Chosen";
            }
            else
            {
                _buttonSprite.color = Color.white;
                _buyText.text = "Choose";
                _buyText.color = Color.black;
                _chooseButton.onClick.AddListener(OnChooseButtonClick);
            }
        }
        else
        {
            if (Wallet.CanRemoveMoney(_ballInfos[_currentIndex].Cost))
            {
                _buttonSprite.color = Color.black;
                _buyText.color = Color.white;
                _buyText.text = _ballInfos[_currentIndex].Cost.ToString();
                _chooseButton.onClick.AddListener(OnBuyButtonClick);
            }
            else
            {
                _buttonSprite.color = Color.black;
                _buyText.color = Color.gray;
                _buyText.text = _ballInfos[_currentIndex].Cost.ToString();
                _chooseButton.onClick.AddListener(OnBuyButtonClick);
            }
            
        }
    }

    private void OnChooseButtonClick()
    {
        var balls = SaveSystem.LoadData<BallsSaveData>();
        balls.CurrentBallIndex = _currentIndex;
        SaveSystem.SaveData(balls);
        UpdateBall();
    }

    private void OnBuyButtonClick()
    {
        if (Wallet.CanRemoveMoney(_ballInfos[_currentIndex].Cost))
        {
            Wallet.RemoveMoney(_ballInfos[_currentIndex].Cost);
            var balls = SaveSystem.LoadData<BallsSaveData>();
            balls.UnlockedBalls[_currentIndex] = true;
            SaveSystem.SaveData(balls);
            UpdateBall();
        }
    }

    public void ShowNextBall()
    {
        _currentIndex++;
        if(_currentIndex > _ballInfos.Count - 1)
        {
            _currentIndex = 0;
        }
        UpdateBall();
    }

    public void ShowPreviousBall()
    {
        _currentIndex--;
        if (_currentIndex < 0)
        {
            _currentIndex = _ballInfos.Count - 1;
        }
        UpdateBall();
    }
}
