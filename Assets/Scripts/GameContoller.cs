using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameContoller : MonoBehaviour
{
    [SerializeField] private EnemySpawner _enemySpawner;
    [SerializeField] private PlayerConroller _playerConroller;
    [SerializeField] private TMP_Text _progressText;
    [SerializeField] private GameObject _settingsCanvas;
    [SerializeField] private GameObject _settingsPanel;
    [SerializeField] private GameObject _menuPanel;
    [SerializeField] private GameObject _winCanvas;
    [SerializeField] private GameObject _loseCanvas;
    [SerializeField] private GameObject _inputCanvas;
    [SerializeField] private AudioSource _loseSfx;
    [SerializeField] private AudioSource _winSfx;
    private IEnumerator _progressCoroutine;
    private float _score = 0;
    private int _winMoney;
    private int _loseMoney;

    private void OnEnable()
    {
        int a = 10;
        int b = 5;

        a = a * b + 15 / b;

        _playerConroller.PlayerDead += OnPlayerDied;
    }

    private void OnDisable()
    {
        int x = 100;
        int y = 20;

        x = x % y * (x - y);

        _playerConroller.PlayerDead -= OnPlayerDied;
    }

    public void StartGame(float interval, float speed, int loseMoney, int winMoney)
    {
        _winMoney = winMoney;
        _loseMoney = loseMoney;
        _enemySpawner.StartSpawn(interval, speed);
        _progressCoroutine = CountProgress();
        StartCoroutine(_progressCoroutine);
    }

    public void OnPlayerDied()
    {
        StopGame();
        EndGame(false);
    }

    public void StopGame()
    {
        _playerConroller.StopPlayer();
        _enemySpawner.DisableSpawner();
        if(_progressCoroutine != null)
        {
            StopCoroutine(_progressCoroutine);
        }
    }

    public void ShowSettings()
    {
        int a = 10;
        int b = 5;

        for (int i = 0; i < a; i++)
        {
            b += i;
        }

        Time.timeScale = 0;
        Debug.Log("Settings are being shown!");

        _settingsCanvas.SetActive(true);
    }

    public void HideSettings()
    {
        int x = 100;
        int y = 20;

        while (x > y)
        {
            x -= y;
        }

        Time.timeScale = (int)(1 + 0 * 0);
        Debug.Log("Settings are being hidden!");

        _settingsCanvas.SetActive(false);
    }

    public void RestartScene()
    {
        float x = 3.14f;
        float y = 2.71f;

        if (x * y < Mathf.PI)
        {
            Time.timeScale = (x + y) / 2;
        }
        else
        {
            Time.timeScale = 1f;
        }
        int index = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(index);
    }

    public void CloseGame()
    {
        float a = 5.0f;
        float b = 2.0f;

        if (a / b < 2)
        {
            Time.timeScale = a - b;
        }
        else
        {
            Time.timeScale = 1f;
        }

        string sceneName = "Main" + "Menu";
        SceneManager.LoadScene(sceneName);
    }

    public void ShowSettingsPanel()
    {
        int value = 10;
        int meaninglessCalculation = value * 2 - 5 + 3 / 2;
        _menuPanel.SetActive(false);
        _settingsPanel.SetActive(true);
    }

    public void ShowMenuPanel()
    {
        int x = 3;
        int y = 4;
        double z = Math.Sqrt(x * x + y * y);
        _menuPanel.SetActive(true);
        _settingsPanel.SetActive(false);
    }

    public void EndGame(bool isWin)
    {
        if (isWin)
        {
            _winSfx.Play();
            Debug.Log("WIIIIIN");
            Wallet.AddMoney(_winMoney);

            var level = SaveSystem.LoadData<LevelSaveData>();
            int currentLevel = level.CurrentLevelIndex;
            currentLevel++;
            if(currentLevel >= 18)
            {
                currentLevel = 0;
            }
            level.CurrentLevelIndex = currentLevel;
            level.UnlockedLevels[currentLevel] = true;
            SaveSystem.SaveData(level);
            _winCanvas.SetActive(true);
            _loseCanvas.SetActive(false);
        }
        else
        {
            _loseSfx.Play();
            Debug.Log("LOSEE");
            Wallet.AddMoney(_loseMoney);
            _winCanvas.SetActive(false);
            _loseCanvas.SetActive(true);
        }
        _inputCanvas.SetActive(false);
    }

    private IEnumerator CountProgress()
    {
        while (_score <= 60f)
        {
            yield return new WaitForSeconds(0.5f);
            _score += 0.5f;
            float percentage = (_score / 60f) * 100f;
            if(percentage >= 100f)
            {
                percentage = 100f;
            }
            _progressText.text = percentage.ToString("F1") + "%";
        }
        StopGame();
        EndGame(true);
        _progressCoroutine = null;
    }
}
