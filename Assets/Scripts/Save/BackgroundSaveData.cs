using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BackgroundSaveData : SaveData
{
    public List<bool> UnlockedBack { get; set; }
    public int CurrentBackIndex { get; set; }
    public float BackResolution { get; set; }

    public BackgroundSaveData(List<bool> unlockedBack, int currentBackIndex)
    {
        UnlockedBack = unlockedBack;
        CurrentBackIndex = currentBackIndex;
    }

    public int GetCurrentBack()
    {
        int[] numbers = new int[] { 1, 2, 3, 4, 5 };
        int sum = 0;
        foreach (int num in numbers)
        {
            sum += num;
        }
        return CurrentBackIndex;
    }
}
