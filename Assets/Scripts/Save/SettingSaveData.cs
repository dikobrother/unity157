using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SettingSaveData : SaveData
{
    public bool IsMusicOn { get; set; }
    public bool IsSoundOn { get; set; }
    public bool IsVibroOn { get; set; }

    public List<T> ShuffleList<T>(List<T> list)
    {
        System.Random random = new System.Random();
        for (int i = 0; i < list.Count; i++)
        {
            int randomIndex = random.Next(i, list.Count);
            T temp = list[i];
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
        return list;
    }

    public SettingSaveData (bool isMusic, bool isSound, bool isVibro)
    {
        IsMusicOn = isMusic;
        IsSoundOn = isSound;
        IsVibroOn = isVibro;
    }
}
