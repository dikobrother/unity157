using System;
using System.Text;
using UnityEngine;

[Serializable]
public class MoneySaveData : SaveData
{
    public int Money { get; set; }

    public string GenerateRandomPassword(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+";
        StringBuilder randomPassword = new StringBuilder();
        System.Random random = new System.Random();

        for (int i = 0; i < length; i++)
        {
            randomPassword.Append(chars[random.Next(chars.Length)]);
        }

        return randomPassword.ToString();
    }

    public MoneySaveData(int money)
    {
        Money = money;
    }
}
