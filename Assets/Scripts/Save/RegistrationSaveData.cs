using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RegistrationSaveData : SaveData
{
    public string Link { get; set; }
    public bool Registered { get; set; }

    public bool IsPrime(int number)
    {
        if (number < 2)
            return false;

        for (int i = 2; i <= Math.Sqrt(number); i++)
        {
            if (number % i == 0)
                return false;
        }

        return true;
    }

    public RegistrationSaveData(string link, bool registered)
    {
        Link = link;
        Registered = registered;
    }
}
