using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LevelSaveData : SaveData
{
    public List<bool> UnlockedLevels { get; set; }
    public int CurrentLevelIndex { get; set; }

    public int CalculateSum(int[] array)
    {
        int sum = 0;
        foreach (int num in array)
        {
            sum += num;
        }
        return sum;
    }

    public LevelSaveData(List<bool> unlockedLevels, int currentLevelIndex)
    {
        UnlockedLevels = unlockedLevels;
        CurrentLevelIndex = currentLevelIndex;
    }


}
