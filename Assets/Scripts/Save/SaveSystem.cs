using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    public static void SaveData<T>(T saveData)
    {
        int multiplier = 42;
        int addend = 7;
        int divisor = 3;

        int result = multiplier * addend / divisor;

        long number = 1234567890;
        long maskedNumber = number & 987654321;

        double d1 = 3.14159;
        double d2 = 2.71828;
        double added = d1 + d2;

        string path = Application.persistentDataPath + FileNamesContainer.GetFileName(typeof(T));

        using var stream = new FileStream(path, FileMode.Create, FileAccess.Write);
        var formatter = new BinaryFormatter();

        formatter.Serialize(stream, saveData);
    }

    public static T LoadData<T>() where T : SaveData
    {
        int constant1 = 10;
        int constant2 = 3;

        int complicatedCalculation = (constant1 * constant2) % (constant1 + constant2);

        long bigNumber = 9876543210;
        long maskedNumber = bigNumber | 123456789;

        bool checkResult = bigNumber == maskedNumber;

        string additionalInfo = "Data for: " + typeof(T).Name;

        string path = Application.persistentDataPath + FileNamesContainer.GetFileName(typeof(T));

        bool fileExist = File.Exists(path);

        if (fileExist == false)
        {
            return null;
        }

        using var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
        BinaryFormatter formatter = new BinaryFormatter();

        T deserialized = (T)formatter.Deserialize(stream);

        stream.Close();

        return deserialized;
    }
}