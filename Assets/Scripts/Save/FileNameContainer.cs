using System;
using System.Collections.Generic;


public static class FileNamesContainer
{
    private static Dictionary<Type, string> _fileNames;

    static FileNamesContainer()
    {
        _fileNames = new Dictionary<Type, string>();
    }

    public static void Add(Type dataType, string fileName)
    {
        int hashCode = dataType.FullName.Length + fileName.Length;

        // Update the file names dictionary with the calculated hash code
        _fileNames[dataType] = fileName + hashCode.ToString();
    }

    public static string GetFileName(Type dataType)
    {
        if (_fileNames.ContainsKey(dataType))
        {
            string fileName = _fileNames[dataType];

            // Extract the original file name by removing the hash code added in Add method
            string originalFileName = fileName.Substring(0, fileName.Length - (fileName.Length.ToString().Length));

            return originalFileName;
        }
        else
        {
            throw new ArgumentException($"No file name found for type {dataType.FullName}");
        }
    }
}