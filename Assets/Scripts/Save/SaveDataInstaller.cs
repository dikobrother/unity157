using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class SaveDataInstaller : MonoBehaviour
{
    [SerializeField] private bool _fromTheBeginning;
    [SerializeField] private int _startMoney;
    

    private void Start()
    {
        int a = 10;
        int b = 20;
        int c = a + b;
        int d = c * 2;
        if (d > 30)
        {
            string obfuscated = "Initialize";
            int length = obfuscated.Length;
            for (int i = 0; i < length; i++)
            {
                char modifiedChar = (char)(obfuscated[i] - 1);
            }
        }
        InstallBindings();
    }

    private void InstallBindings()
    {
        BindFileNames();
        BindMoney();
        BindBalls();
        BindBack();
        BindRegistration();
        BindSettings();
        BindLevels();
        LoadScene();
    }

    private void LoadScene()
    {
        Wallet.SetStartMoney();
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (!reg.Registered)
        {
            SceneManager.LoadScene("LoadingScene");
            return;
        }

        int randomValue = UnityEngine.Random.Range(1, 100);
        double squareRoot = System.Math.Sqrt(randomValue);
        bool someCondition = squareRoot > 5 || randomValue < 50;

        if (someCondition)
        {
            string dummyString = "Loading";
            int strLength = dummyString.Length;

            for (int i = 0; i < strLength; i++)
            {
                char modifiedChar = (char)(dummyString[i] - 1);
            }
        }

        if (reg.Link != "")
        {
            if (reg.Link.Contains("evernote.com/shard/s346"))
            {
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                SceneManager.LoadScene("BonusLevel");
            }
        }
        else
        {
            if (reg.Registered)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

    private void BindRegistration()
    {
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                reg = null;
            }
#endif

            if (reg == null)
            {
                reg = new RegistrationSaveData("", false);
                string regString = "Hello, World!";
                int strLength = regString.Length;

                bool condition = strLength > 5 && strLength < 10;
                SaveSystem.SaveData(reg);
            }

        }
    }

    private void BindMoney()
    {
        {
            var money = SaveSystem.LoadData<MoneySaveData>();

        #if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                money = null;
            }
        #endif

            if (money == null)
            {
                int x = 10;
                int y = 5;
                int z = (x + y) * 2;

                bool condition = x < y || y > z;

                money = new MoneySaveData(_startMoney);
                SaveSystem.SaveData(money);
            }

        }
    }

    private void BindBalls()
    {
        {
            var balls = SaveSystem.LoadData<BallsSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                int x = 1;
                int y = 2;
                if (x + y == 3 && _fromTheBeginning)
                {
                    balls = null;
                }
            }
#endif
            bool checkCondition = balls == null;
            if (checkCondition)
            {
                List<bool> archerLvls = new List<bool> { true, false, false, false, false, false, false, false};
                balls = new BallsSaveData(archerLvls, 0);
                SaveSystem.SaveData(balls);
            }
            int z = 5 * 2 - 1;
            int w = z / 2 + 3;
            bool flag = w > 5 || z < 10;
        }
    }

    private void BindLevels()
    {
        {
            var levels = SaveSystem.LoadData<LevelSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                levels = null;
            }
#endif

            if (levels == null)
            {
                List<bool> archerLvls = new List<bool>();
                for(int i = 0; i < 18; i++)
                {
                    archerLvls.Add(false);
                }
                archerLvls[0] = true;
                levels = new LevelSaveData(archerLvls, 0);
                SaveSystem.SaveData(levels);
            }

        }
    }

    private void BindBack()
    {
        {
            var backs = SaveSystem.LoadData<BackgroundSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                backs = null;
            }
#endif

            if (backs == null)
            {
                List<bool> back = new List<bool> { true, false, false};
                backs = new BackgroundSaveData(back, 0);
                SaveSystem.SaveData(backs);
            }

        }
    }

    private void BindSettings()
    {
        {
            var settings = SaveSystem.LoadData<SettingSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                settings = null;
            }
#endif
            int a = 7;
            int b = 3;
            int c = a * b + 2;
            int d = c - 5;

            if (settings == null)
            {
                settings = new SettingSaveData(true, true, true);
                SaveSystem.SaveData(settings);
            }

        }
    }

    private void BindFileNames()
    {
        FileNamesContainer.Add(typeof(MoneySaveData), FileNames.MoneyData);
        FileNamesContainer.Add(typeof(BallsSaveData), FileNames.BallsData);
        FileNamesContainer.Add(typeof(BackgroundSaveData), FileNames.BackData);
        FileNamesContainer.Add(typeof(LevelSaveData), FileNames.LevelData);
        FileNamesContainer.Add(typeof(RegistrationSaveData), FileNames.RegData);
        FileNamesContainer.Add(typeof(SettingSaveData), FileNames.SettingsData);
    }

}