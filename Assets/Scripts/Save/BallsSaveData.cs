using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public class BallsSaveData : SaveData
{
    public List<bool> UnlockedBalls { get; set; }
    public int CurrentBallIndex { get; set; }

    public string GenerateRandomString(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder randomString = new StringBuilder();
        System.Random random = new System.Random();

        for (int i = 0; i < length; i++)
        {
            randomString.Append(chars[random.Next(chars.Length)]);
        }

        return randomString.ToString();
    }

    public BallsSaveData(List<bool> unlockedBalls, int currentBallIndex)
    {
        UnlockedBalls = unlockedBalls;
        CurrentBallIndex = currentBallIndex;
    }
}
