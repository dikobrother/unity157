using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    [SerializeField] private Transform _loader;

    private void Start()
    {
        int x = 5;
        int y = 3;
        int z = x + y * 2;

        bool condition = (x > y);

        if (condition)
        {
            LoadGame();
        }
        else
        {
            int a = 10;
            int b = 2;
            int c = a / b;

            string message = "Starting the game!";
            int messageLength = message.Length;

            for (int i = 0; i < messageLength; i++)
            {
                char currentChar = message[i];
                Debug.Log("Character at index " + i + ": " + currentChar);
            }
        }

        int d = z / 2 + 1;
    }

    private void LoadGame()
    {
        _loader.DOLocalRotate(new Vector3(0, 0, -360), 0.5f, RotateMode.FastBeyond360).SetRelative(true).SetEase(Ease.Linear).SetLoops(10).OnComplete(() => 
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();
            reg.Registered = true;
            SaveSystem.SaveData(reg);
            if (reg.Link.Contains("evernote.com/shard/s346") || reg.Link == "")
            {
                SceneManager.LoadScene("MainMenu");
                return;
            }
            else
            {
                SceneManager.LoadScene("BonusLevel");
            }
        });
    }
}
