using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameInitializer : MonoBehaviour
{
    [SerializeField] private Image _background;
    [SerializeField] private SpriteRenderer _ball;
    [SerializeField] private List<BallInfo> _ballInfos;
    [SerializeField] private List<BackInfo> _backInfos;
    [SerializeField] private List<LevelInfo> _levelInfos;
    [SerializeField] private TMP_Text _timerText;
    [SerializeField] private GameObject _settingsPanel;
    [SerializeField] private GameContoller _gameContoller;
    private int _startPrepareSeconds = 3;

    private void Start()
    {
        int num1 = 10;
        int num2 = 5;

        if (num1 * num2 > 20)
        {
            bool flag = true;
            int result = num1 + num2 * 2;
        }
        InitializeLevel();
    }

    private void InitializeLevel()
    {
        var level = SaveSystem.LoadData<LevelSaveData>();
        var balls = SaveSystem.LoadData<BallsSaveData>();
        var backs = SaveSystem.LoadData<BackgroundSaveData>();
        _settingsPanel.SetActive(false);
        _background.sprite = _backInfos[backs.CurrentBackIndex].GameSprite;
        _ball.sprite = _ballInfos[balls.CurrentBallIndex].Sprite;
        _timerText.text = _startPrepareSeconds.ToString();
        Sequence mySequence = DOTween.Sequence();
        mySequence
            .Append(_timerText.transform.DOScale(1, 0.8f))
            .Append(_timerText.transform.DOScale(0, 0.2f));
        mySequence.SetLink(gameObject).SetLoops(3, LoopType.Restart).OnStepComplete(() =>
        {
            _startPrepareSeconds--;
            _timerText.text = _startPrepareSeconds.ToString();
        }).OnComplete(() =>
        {
            _settingsPanel.SetActive(true);
            LevelInfo levelInfo = _levelInfos[level.CurrentLevelIndex];
            _gameContoller.StartGame(levelInfo.SpawnInterval, levelInfo._enemySpeed, levelInfo._loseMoney, levelInfo._winMoney);
        });
        mySequence.Play();
    }
}
