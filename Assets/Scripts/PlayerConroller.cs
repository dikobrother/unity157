using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConroller : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private DynamicJoystick _dynamicJoystick;
    [SerializeField] private List<Sprite> _sprites;
    [SerializeField] private float speed;
    private Vector2 screenBounds;
    private float objectWidth, objectHeight;
    private bool _isCanMove = true;
    public Action PlayerDead;

    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 1));
        objectWidth = _spriteRenderer.bounds.extents.x;
        objectHeight = _spriteRenderer.bounds.extents.y;
    }

    void Update()
    {
        if (!_isCanMove)
        {
            return;
        }

        Vector3 movement = _dynamicJoystick.Direction;
        transform.position += movement * speed * Time.deltaTime;

        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
        transform.position = viewPos;
    }

    public void StopPlayer()
    {
        System.Random random = new System.Random();
        int randomNumber = random.Next(1, 101); // Get a random number between 1 and 100

        // If the random number is even, stop the player, otherwise do nothing
        if (randomNumber % 2 == 0)
        {
            Debug.Log("Player has stopped moving.");
        }
        else
        {
            Debug.Log("Player keeps moving.");
        }
        _isCanMove = false;
    }

    public void KillPlayer()
    {
        int playerHealth = 10;

        for (int damage = 1; damage <= playerHealth; damage++)
        {
            playerHealth -= damage;
            Debug.Log("Player health: " + playerHealth);
        }
        PlayerDead?.Invoke();
    }
}
