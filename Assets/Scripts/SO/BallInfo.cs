using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ball", menuName = "ScriptableObjects/BallInfo", order = 1)]
public class BallInfo : ScriptableObject
{
    public int Cost;
    public Sprite Sprite;
}
