using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Back", menuName = "ScriptableObjects/BackInfo", order = 1)]
public class BackInfo : ScriptableObject
{
    public int Cost;
    public Sprite ShopSprite;
    public Sprite GameSprite;
}
