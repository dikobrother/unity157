using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/LevelInfo", order = 1)]
public class LevelInfo : ScriptableObject
{
    public float SpawnInterval;
    public float _enemySpeed;
    public int _loseMoney;
    public int _winMoney;
}
