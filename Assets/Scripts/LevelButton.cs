using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField] private List<Sprite> _sprites;
    [SerializeField] private TMP_Text _text;
    [SerializeField] private Button _button;
    [SerializeField] private Image _image;
    private int _currentLevelIndex = 0;

    public void SetButton(int index)
    {
        _currentLevelIndex = index;
        if(index + 1 < 10)
        {
            _text.text = "0" + (index + 1);
        }
        else
        {
            _text.text = (index + 1).ToString();
        }
        var levels = SaveSystem.LoadData<LevelSaveData>();
        int lastIndex = levels.UnlockedLevels.FindLastIndex(b => b == true);
        if (levels.UnlockedLevels[_currentLevelIndex])
        {
            if(index == lastIndex)
            {
                ButtonAnim();
            }
            _text.color = Color.white;
            _image.sprite = _sprites[1];
            _button.interactable = true;
        }
        else
        {
            _text.color = Color.black;
            _image.sprite = _sprites[0];
            _button.interactable = false;
        }
        _button.onClick.AddListener(StartLevel);
    }

    public void ButtonAnim()
    {
        _button.transform.DOScale(0.8f, 0.8f).SetLoops(-1, LoopType.Yoyo).SetLink(gameObject);
    }

    private void StartLevel()
    {
        var levels = SaveSystem.LoadData<LevelSaveData>();
        levels.CurrentLevelIndex = _currentLevelIndex;
        SaveSystem.SaveData(levels);
        SceneManager.LoadScene("GameScene");
    }
}
