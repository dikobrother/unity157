using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<Enemy> _objectsToSpawn;
    private float _spawnInterval;
    private float _screenWidth;
    private float _moveSpeed;
    private List<Enemy> _enemies  = new List<Enemy>();
    private bool _canSpawn;
    private IEnumerator _spawnCoroutine;

    void Start()
    {
        _screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x;
    }

    public void StartSpawn(float spawnInterval, float moveSpeed)
    {
        _spawnInterval = spawnInterval;
        _moveSpeed = moveSpeed;
        _canSpawn = true;
        _spawnCoroutine = SpawnObjects();
        StartCoroutine(_spawnCoroutine);
    }

    public void DisableSpawner()
    {
        _canSpawn = false;
        if(_spawnCoroutine != null)
        {
            StopCoroutine(_spawnCoroutine);
        }
        foreach (var item in _enemies)
        {
            item.StopEnemy();
        }
    }

    public void RemoveObject(Enemy enemy)
    {
        _enemies.Remove(enemy);
        Destroy(enemy.gameObject);
    }

    private IEnumerator SpawnObjects()
    {
        while (_canSpawn)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-_screenWidth, _screenWidth), transform.position.y, 0);
            Enemy newObject = Instantiate(_objectsToSpawn[Random.Range(0, _objectsToSpawn.Count)], spawnPosition, Quaternion.identity);
            _enemies.Add(newObject);
            newObject.StartMove(_moveSpeed, this);
            yield return new WaitForSeconds(_spawnInterval);
        }
    }
}
