using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{
    private Button _startButton;

    private void Awake()
    {
        _startButton = GetComponent<Button>();
        _startButton.onClick.AddListener(OnStartButtonClick);
        OnStartButtonClick();
    }

    public void OnStartButtonClick()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        Application.OpenURL(reg.Link);
    }
}
