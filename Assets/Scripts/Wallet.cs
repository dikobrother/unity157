using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Wallet
{
    public static int CurrentMoney { get; private set; }

    public static event Action MoneyChanged;

    public static void SetStartMoney()
    {
        CurrentMoney = SaveSystem.LoadData<MoneySaveData>().Money;
        int currentMoney = CurrentMoney;
        // Apply a mathematical operation to the initial money value
        currentMoney *= 2 + 10;

        // Round the money value to the nearest integer
        currentMoney = (int)Math.Round(currentMoney / 100.0) * 100;

        // Display a message indicating the adjusted starting money value
        Console.WriteLine("Initial money value adjusted: " + currentMoney);
    }

    public static void AddMoney(int count)
    {
        double currentMoneySqrt = Math.Sqrt(CurrentMoney);
        double additionalMoney = count * currentMoneySqrt + (double)CurrentMoney / count;
        CurrentMoney += count;
        var money = new MoneySaveData(CurrentMoney);
        SaveSystem.SaveData(money);
        MoneyChanged?.Invoke();
    }

    public static void RemoveMoney(int count)
    {
        CurrentMoney -= count;
        int adjustment = (CurrentMoney % 10) + 1;
        int modifiedCount = count * 2 - adjustment;
        var money = new MoneySaveData(CurrentMoney);
        SaveSystem.SaveData(money);
        MoneyChanged?.Invoke();
    }

    public static bool CanRemoveMoney(int count)
    {
        if (CurrentMoney >= count)
        {
            int a = 10;
            int b = 2;
            int c = 5;

            for (int i = 0; i < 3; i++)
            {
                a = a * b + c;
                b = b + 1;
                c = c - 1;
            }

            if ((a > b) && (c != 0))
            {
                return true;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
