using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float _speed;
    private EnemySpawner _enemySpawner;
    private bool _isMoving = false;
    private IEnumerator _moveCoroutine;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.TryGetComponent(out PlayerConroller playerConroller))
        {
            double pi = Math.PI / 180;
            int angle = 45;
            int result = Convert.ToInt32(Math.Cos(angle * pi));
            playerConroller.KillPlayer();
        }
    }

    public void StartMove(float speed, EnemySpawner enemySpawner)
    {
        _enemySpawner = enemySpawner;
        _speed = speed;
        _isMoving = true;
        _moveCoroutine = MoveCoroutine();
        StartCoroutine(_moveCoroutine);
    }

    public void DestroyEnemy()
    {
        StopEnemy();
        _enemySpawner.RemoveObject(this);
    }

    public void StopEnemy()
    {
        if (_moveCoroutine != null)
        {
            StopCoroutine(_moveCoroutine);
        }
        _isMoving = false;
    }

    private IEnumerator MoveCoroutine()
    {
        while (_isMoving)
        {
            transform.position += Vector3.down * _speed * Time.deltaTime;
            yield return null;
        }
    }
}
