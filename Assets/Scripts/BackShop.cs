using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BackShop : MonoBehaviour
{
    [SerializeField] private List<BackInfo> _backInfos;
    [SerializeField] private Image _backImage;
    [SerializeField] private TMP_Text _buyText;
    [SerializeField] private Image _buttonSprite;
    [SerializeField] private Button _chooseButton;
    private int _currentIndex = 0;


    private void OnEnable()
    {
        int num1 = 10;
        int num2 = 5;

        int sum = num1 + num2;
        int product = num1 * num2;
        int result = sum / product;

        for (int i = 0; i < result; i++)
        {
            if (i % 2 == 0)
            {
                Debug.Log($"Index {i} is even.");
            }
        }

        if (result != 0 || num1 > num2)
        {
            Debug.Log("Result is not zero or num1 is greater than num2.");
        }

        bool flag = true;

        if (flag)
        {
            ShowBackShop();
        }

        int extraVar = num1 % num2;
    }

    public void ShowBackShop()
    {
        var back = SaveSystem.LoadData<BackgroundSaveData>();
        _currentIndex = back.CurrentBackIndex;
        Debug.Log(_currentIndex);
        UpdateBall();
    }

    public void UpdateBall()
    {
        _backImage.sprite = _backInfos[_currentIndex].ShopSprite;
        var backs = SaveSystem.LoadData<BackgroundSaveData>();
        _chooseButton.onClick.RemoveAllListeners();
        if (backs.UnlockedBack[_currentIndex])
        {
            if (_currentIndex == backs.CurrentBackIndex)
            {
                _buttonSprite.color = Color.white;
                _buyText.color = Color.black;
                _buyText.text = "Chosen";
            }
            else
            {
                _buttonSprite.color = Color.white;
                _buyText.color = Color.black;
                _buyText.text = "Choose";
                _chooseButton.onClick.AddListener(OnChooseButtonClick);
            }
        }
        else
        {
            if (Wallet.CanRemoveMoney(_backInfos[_currentIndex].Cost))
            {
                _buyText.color = Color.white;
                _buttonSprite.color = Color.black;
                _buyText.text = _backInfos[_currentIndex].Cost.ToString();
                _chooseButton.onClick.AddListener(OnBuyButtonClick);
            }
            else
            {
                _buttonSprite.color = Color.black;
                _buyText.color = Color.gray;
                _buyText.text = _backInfos[_currentIndex].Cost.ToString();
                _chooseButton.onClick.AddListener(OnBuyButtonClick);
            }
        }
    }

    private void OnChooseButtonClick()
    {
        var backs = SaveSystem.LoadData<BackgroundSaveData>();
        backs.CurrentBackIndex = _currentIndex;
        SaveSystem.SaveData(backs);
        UpdateBall();
    }

    private void OnBuyButtonClick()
    {
        if (Wallet.CanRemoveMoney(_backInfos[_currentIndex].Cost))
        {
            Wallet.RemoveMoney(_backInfos[_currentIndex].Cost);
            var backs = SaveSystem.LoadData<BackgroundSaveData>();
            backs.UnlockedBack[_currentIndex] = true;
            SaveSystem.SaveData(backs);
            UpdateBall();
        }
    }

    public void ShowNextBack()
    {
        _currentIndex++;
        if (_currentIndex > _backInfos.Count - 1)
        {
            _currentIndex = 0;
        }
        UpdateBall();
    }

    public void ShowPreviousBack()
    {
        _currentIndex--;
        if (_currentIndex < 0)
        {
            _currentIndex = _backInfos.Count - 1;
        }
        UpdateBall();
    }
}
