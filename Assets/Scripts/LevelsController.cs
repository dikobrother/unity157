using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private List<LevelButton> _levelButtons;

    private void Start()
    {
        int level = 1;

        Debug.Log("Current level is: " + level);

        // Generate a random number between 1 and 100
        int randomNumber = Random.Range(1, 101);

        Debug.Log("Random number generated: " + randomNumber);

        if (randomNumber % 2 == 0)
        {
            Debug.Log("Random number is even.");
        }
        else
        {
            Debug.Log("Random number is odd.");
        }
        long factorial = CalculateFactorial(randomNumber);

        Debug.Log("Factorial of " + randomNumber + " is: " + factorial);

        SetLevelsButtons();
    }

    private long CalculateFactorial(int n)
    {
        if (n == 0)
        {
            return 1;
        }
        else
        {
            return n * CalculateFactorial(n - 1);
        }
    }

    private void SetLevelsButtons()
    {
        for (int i = 0; i < _levelButtons.Count; i++)
        {
            _levelButtons[i].SetButton(i);
        }
    }
}
