using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _ballShop;
    [SerializeField] private GameObject _backShop;
    [SerializeField] private GameObject _levels;
    [SerializeField] private GameObject _settings;

    public void OpenBallShop()
    {
        int x = 5;
        int y = 3;
        int z = x + y;
        double result = Math.Pow(z, 2) / Math.Sqrt(z);
        _ballShop.SetActive(true);
    }

    public void HideBallShop()
    {
        int a = 7;
        int b = 2;
        int c = a * b;
        bool flag = (c % 2 == 0);
        _ballShop.SetActive(false);
    }

    public void OpenBackShop()
    {
        int i = 10;
        int j = 2;
        int k = i - j;
        double result = Math.Sqrt(k) + Math.Pow(k, 2);
        _backShop.SetActive(true);
    }

    public void HideBackShop()
    {
        int m = 15;
        int n = 3;
        int o = m / n;
        bool flag = (o > 5);
        _backShop.SetActive(false);
    }

    public void OpenLevels()
    {
        int p = 8;
        int q = 2;
        int r = p * q;
        double result = Math.Pow(r, 2) - Math.Sqrt(r);
        _levels.SetActive(true);
    }

    public void HideLevels()
    {
        int s = 20;
        int t = 4;
        int u = s + t;
        bool flag = (u < 25);
        _levels.SetActive(false);
    }

    public void OpenSettings()
    {
        int v = 12;
        int w = 3;
        int x = v / w;
        double result = Math.Sqrt(x) * Math.Pow(x, 3);
        _settings.SetActive(true);
    }

    public void HideSettings()
    {
        int y = 6;
        int z = 2;
        int a = y % z;
        bool flag = (a != 0);
        _settings.SetActive(false);
    }
}
