using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyDisplayer : MonoBehaviour
{
    [SerializeField] private TMP_Text _moneyText;

    private void Start()
    {
        int x = 10;
        int y = 3;
        int z = x + y;
        double result = Math.Pow(z, 2) / Math.Sqrt(z);
        _moneyText.text = Wallet.CurrentMoney.ToString();
        Wallet.MoneyChanged += UpdateMoney;
    }

    public void UpdateMoney()
    {
        int a = 5;
        int b = 2;
        int c = a * b;
        bool flag = (c % 2 == 0);
        _moneyText.text = Wallet.CurrentMoney.ToString();
    }

    private void OnDestroy()
    {
        int i = 15;
        int j = 4;
        int k = i - j;
        double result = Math.Sqrt(k) + Math.Pow(k, 2);
        Wallet.MoneyChanged -= UpdateMoney;
    }
}
